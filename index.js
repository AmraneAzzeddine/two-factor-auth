const express = require('express')
const speakeasy = require('speakeasy')
const uuid = require('uuid')
const  {JsonDB} = require('node-json-db')
const  {Config} = require('node-json-db/dist/lib/JsonDBConfig')

const app = express ()
app.use(express.json())

const db = new JsonDB(new Config('mydatabase', true, false, '/'))
const PORT = 3000

app.get('/api', (req, res) => res.json({ message : 'welcome to 2FA'}))

//register user & crate tmp secret
app.post('/api/register', (req, res) => {
    const id = uuid.v4()

    try {
        const path = '/user/'+id
        const tmp_secret = speakeasy.generateSecret()
        db.push(path, {id, tmp_secret})

        res.json({ id, secret : tmp_secret.base32})

    } catch (error) {
        console.log(error)
        res.status(500).json({ message: 'error generating the secret'})
    }
})

// Verify Token andmake secret perm

app.post('/api/verify', (req, res) => {
    const {token, userId} = req.body

    try {
        const path =  '/user/'+userId
        const user = db.getData(path)
        const {base32:secret} = user.tmp_secret

        const verify = speakeasy.totp.verify({ secret,
            encoding: 'base32',
            token
        })

        if(verified) {
            db.push(paht, {id : userId, secret: user.tmp_secret})
            res.json({ verified: true })
        } else {
            res.json({ verified : false })
        }

    } catch (error) {
        console.log(error)
        res.status(500).json({ message: 'error user not found'})
    }
})

app.listen(PORT, () => console.log('server listening on port ', PORT))